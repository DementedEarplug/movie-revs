-- Insert Producers
INSERT INTO producer (name) VALUES
    ('ProducerA'),
    ('ProducerB'),
    ('ProducerC'),
    ('ProducerD');

-- Insert Movies
INSERT INTO movie (name, producer_id) VALUES
    ('Movie 11', 1),
    ('Movie 12', 1),
    ('Movie 21', 2),
    ('Movie 22', 2),
    ('Movie 23', 2),
    ('Movie 31', 3),
    ('Movie 32', 3),
    ('Movie 33', 3),
    ('Movie 34', 3),
    ('Movie 35', 3);

-- Insert Reviewers
INSERT INTO reviewer (name) VALUES
    ('Reviewer X'),
    ('Reviewer Y'),
    ('Reviewer Z');

-- Insert Reviews
INSERT INTO movie_review (movie_id, reviewer_id, rating) VALUES
    (1, 1, 8),
    (1, 2, 9),
    (2, 1, 7),
    (2, 2, 6),
    (6, 2, 6),
    (3, 2, 6),
    (2, 3, 6),
    (4, 3, 6),
    (6, 1, 6),
    (5, 3, 6),
    (7, 3, 6),
    (3, 2, 10);