import { useState } from "react";
import MovieList from "@/components/MovieList";
import Nav from "@/components/Navbar";
import "./App.css";

function App() {
  const [movieList, setMovieList] = useState([]);
  const [searchValue, setSearchValue] = useState("");

  const handleSearch = async (e) => {
    e.preventDefault();
    try {
      if (searchValue) {
        const data = await fetch(
          `http://localhost:8080/movies/by-producer/${searchValue}`
        );
        const matchingMovies = await data.json();
        setMovieList(matchingMovies);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleTextChange = (event) => {
    setSearchValue(event.target.value);
  };

  const clearSearch = () => {
    setSearchValue("");
  };

  const showEmptyResultsMessage = (
    <div className="text-gray-700 font-medium text-xl">
      There are no movie entries for this producer...
    </div>
  );

  const showSearchPromptMessage = (
    <div className="text-gray-700 font-medium text-xl">
      Search for a producer to see their movies...
    </div>
  );

  return (
    <>
      <header>
        <Nav />
      </header>
      <section className="px-10">
        <h1 className="text-3xl font-bold text-slate-800">Movie Reviews</h1>
        <div className="max-w-lg pt-8">
          <form className="flex justify-between w-full" onSubmit={handleSearch}>
            <div className="relative w-3/4">
              <input
                type="text"
                name="search"
                id="search"
                placeholder="Search movies by producer name"
                onChange={(e) => handleTextChange(e)}
                value={searchValue}
                className="px-4 w-full h-12 border-2 border-indigo-950 rounded-lg"
              />
              {searchValue && (
                <button
                  type="button"
                  className="absolute top-1/2 right-3 transform -translate-y-1/2"
                  onClick={clearSearch}
                >
                  {/* SVG icon */}
                </button>
              )}
            </div>
            <button
              className="bg-indigo-600 rounded-lg py-1 text-white font-semibold w-1/6 disabled:opacity-50"
              type="submit"
              disabled={searchValue === ""}
            >
              Search
            </button>
          </form>
        </div>
        <div className="pt-8">
          {(() => {
            if (movieList.length > 0) {
              return <MovieList movieList={movieList} />;
            } else if (searchValue === "") {
              return showSearchPromptMessage;
            } else {
              return showEmptyResultsMessage;
            }
          })()}
        </div>
      </section>
    </>
  );
}

export default App;
