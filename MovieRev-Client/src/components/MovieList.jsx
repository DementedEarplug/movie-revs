import React, { useState } from "react";
import Movie from "./Movie";

const MovieList = ({ movieList }) => {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
      {movieList.map((movie) => (
        <Movie
          key={movie.id}
          name={movie.name}
          producerName={movie.producerName}
          reviews={movie.reviews}
        />
      ))}
    </div>
  );
};

export default MovieList;
