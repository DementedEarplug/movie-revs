import React from "react";

const Movie = ({ name, producerName, reviews }) => {
  return (
    <div className="bg-white border rounded-lg shadow-lg p-4  ">
      <h2 className="text-xl font-semibold mb-2">{name}</h2>
      <h3 className="text-gray-600">Produced by: {producerName}</h3>
      <div className="mt-4">
        {reviews.length > 0 ? (
          <>
            <h4 className="text-lg font-medium mb-2">Reviews:</h4>
            <ul>
              {reviews.map((review, index) => (
                <li key={index} className="mb-2">
                  <span className="font-semibold">Reviewer:</span>{" "}
                  {review.reviewerName}
                  <br />
                  <span className="font-semibold">Rating:</span> {review.rating}
                  /10
                </li>
              ))}
            </ul>
          </>
        ) : (
          <h4 className="text-md font-normal mb-2">This movie has not been reviewed yet</h4>
        )}
      </div>
    </div>
  );
};

export default Movie;
