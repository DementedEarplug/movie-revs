"use client";

import { useState, useEffect } from "react";

const Nav = () => {


  return (
    <nav className="flex-between w-full mb-16 py-3 bg-indigo-700">
        <p className="text-xl text-white font-bold px-3">MovieRevs</p>
    </nav>
  );
};

export default Nav;
