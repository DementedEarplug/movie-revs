
## Learning from Join Tables

Initially, my database schema included tables for `movie`, `producer`, and `reviewer`, with inter-table relationships that were not effectively structured. With Dana's recommendation about looking into joind tables I realized the limitations of this design and how join tables could help improve the design providing better entities once in the Spring Entities definitions.

### Original Tables:
- `movie`
  - id (pk)
  - name

- `producer`
  - id (pk)
  - name
  - movie_id (fk)

- `reviewer`
  - id (pk)
  - name
  - movie_id (fk)
  - movie_rating

## Improved Structure

Improved structure using join tables.
### Tables:
- `movie`
  - id (pk)
  - name 
  - producer_id (fk)

- `producer`
  - id (pk)
  - name 

- `reviewer`
  - id (pk)
  - name 

- `movie_review`
  - id (pk)
  - movie_rating 
  - movie_id (fk)
  - reviewer_id (fk) 

## Leveraging DTOs for Complex Queries

Utilizing join tables and my improved Spring entities, I was able to achieve the desired query functionality of retrieving all movies produced by a specific producer, along with reviewer names and review ratings. To seamlessly merge and display this data in JSON format while preventing circular dependencies, I implemented some DTOs to help streamline the API response.

## Java version questions
I was also asked about specific Java Features. I looked at some of the code I've written in some pf my other projects to see If I could Identify some instances where I used "newer" java features.

As I mentioned in the interview I used the Lamba notation one of the cases (which I applied in this solutions) what when using find functions that return Optinals

Here I would lamdas thro trhow an error when an element was not found. something like this:
```java
        Producer producerFound = producerRepository.findByName(producerName).orElseThrow(()-> new NoSuchElementException("Producer with name: "+ producerName +" was not found"));

```
Im guessing `optionals` are also a new java concept that I've used before.

Another concept is the use of `::` to call static methods which Im using to call the movie mapper mapToDtO static function within my service.
```java
return matchingMovies.stream()
                .map(MovieMapper::mapToDTO)
                .collect(Collectors.toList());
```

## Full implementation
I this repository I added the full implementation for all of the classes that we briefly rushed over during the interview but witht the revised understanding of the table design for the entities. I included I docker container and some seed data to create a MySQL db.

Available Producers to search for are:
- ProducerA
- ProducerB
- ProducerC
- ProducerD (no movies)

### UI
The UI is a simple search box where you put in the producer name and it returns a list of cards that contains the movies by that producers and their reviews:

![Alt text](image.png)

If you have not searches anything or the search box is empty you get the following message.

![Alt text](image-1.png)

When you have a hit you can see all the movies the producer has worked on along with their reviews.

![Alt text](image-2.png)

If the producer has no movies or does not exist you get this message. (which should prpbably be improved to have a message that the producer does not exist)