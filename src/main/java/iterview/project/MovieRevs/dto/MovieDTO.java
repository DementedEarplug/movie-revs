package iterview.project.MovieRevs.dto;

import java.util.List;

public class MovieDTO {
    private Long id;
    private String name;
    private String producerName;
    private List<MovieReviewDTO> reviews;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    public List<MovieReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(List<MovieReviewDTO> reviews) {
        this.reviews = reviews;
    }
}