package iterview.project.MovieRevs.dto;

import java.util.List;
import java.util.stream.Collectors;

import iterview.project.MovieRevs.entities.Movie;
import iterview.project.MovieRevs.entities.MovieReview;

public class MovieMapper {
    // Map movies to a DTO
    public static MovieDTO mapToDTO(Movie movie) {
        MovieDTO dto = new MovieDTO();
        dto.setId(movie.getId());
        dto.setName(movie.getName());
        dto.setProducerName(movie.getProducer().getName()); // Just need the name not the whole producer object
        
        // Convert reviews into DTOs to avoid circular dependencies during serialization
        List<MovieReviewDTO> reviewDTOs = movie.getReviews().stream()
            .map(review -> mapReviewToDTO(review))
            .collect(Collectors.toList());
        
        dto.setReviews(reviewDTOs);
        return dto;
    }

    // Map a review to a DTO
    private static MovieReviewDTO mapReviewToDTO(MovieReview review) {
        MovieReviewDTO reviewDTO = new MovieReviewDTO();
        reviewDTO.setReviewerName(review.getReviewer().getName());
        reviewDTO.setRating(review.getRating());
        return reviewDTO;
    }
}
