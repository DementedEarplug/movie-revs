package iterview.project.MovieRevs.api;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import iterview.project.MovieRevs.business.MovieService;
import iterview.project.MovieRevs.dto.MovieDTO;

@RestController
@RequestMapping("/movies")
public class MovieController {
    @Autowired
    MovieService movieService;

    @GetMapping("/by-producer/{producerName}")
    public ResponseEntity<List<MovieDTO>> getByProducerName(@PathVariable("producerName") String producerName){
        List<MovieDTO> matchingMovies = new ArrayList<>();
        try {
            matchingMovies = movieService.getMoviesByProducerName(producerName);
            return new ResponseEntity<>(matchingMovies,  HttpStatus.OK);
        } catch (NoSuchElementException e) {
            //Do propper Logging og the error here.
            return new ResponseEntity<>(matchingMovies , HttpStatus.NOT_FOUND);
        }

    }
}
