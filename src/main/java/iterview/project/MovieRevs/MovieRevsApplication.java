package iterview.project.MovieRevs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieRevsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieRevsApplication.class, args);
	}

}
