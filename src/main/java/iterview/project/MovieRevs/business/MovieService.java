package iterview.project.MovieRevs.business;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import iterview.project.MovieRevs.dao.MovieRepository;
import iterview.project.MovieRevs.dao.ProducerRepository;
import iterview.project.MovieRevs.dto.MovieDTO;
import iterview.project.MovieRevs.dto.MovieMapper;
import iterview.project.MovieRevs.entities.Movie;
import iterview.project.MovieRevs.entities.Producer;

@Service
public class MovieService {
    @Autowired
    MovieRepository movieRepository;

    @Autowired
    ProducerRepository producerRepository;

    public List<MovieDTO> getMoviesByProducerName(String producerName) {
        List<Movie> matchingMovies = new ArrayList<>();
        // Check if producer exists
        Producer producerFound = producerRepository.findByName(producerName).orElseThrow(
                () -> new NoSuchElementException("Producer with name: " + producerName + " was not found"));

        if (producerFound != null) {
            matchingMovies = movieRepository.findByProducerName(producerName);
        }
        //Map the matching movies to a DTO
        return matchingMovies.stream()
                .map(MovieMapper::mapToDTO)
                .collect(Collectors.toList());
    }
}
