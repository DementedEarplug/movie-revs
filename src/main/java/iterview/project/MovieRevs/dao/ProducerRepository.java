package iterview.project.MovieRevs.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import iterview.project.MovieRevs.entities.Producer;

public interface ProducerRepository extends JpaRepository<Producer, Long>{

    public Optional<Producer> findByName(String name);
    
}
