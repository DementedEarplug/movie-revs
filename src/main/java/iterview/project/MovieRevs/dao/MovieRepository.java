package iterview.project.MovieRevs.dao;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import iterview.project.MovieRevs.entities.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    @EntityGraph(attributePaths = {"producer", "reviews", "reviews.reviewer"})
    List<Movie> findByProducerName( String producerName);
}
