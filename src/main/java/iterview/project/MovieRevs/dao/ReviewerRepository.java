package iterview.project.MovieRevs.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import iterview.project.MovieRevs.entities.Reviewer;

public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{
    
}
