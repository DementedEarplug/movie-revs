package iterview.project.MovieRevs.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import iterview.project.MovieRevs.entities.MovieReview;

public interface MovieReviewsRepository extends JpaRepository<MovieReview, Long>{
    
}
